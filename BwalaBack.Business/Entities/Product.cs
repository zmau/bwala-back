﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BwalaBack.Business.Entities
{
    public class Product
    {
        public int ID { get; set; }
        public int CategoryID { get; set; }
        public string Name { get; set; }
        public string MeasurementUnit { get; set; }
    }
}
