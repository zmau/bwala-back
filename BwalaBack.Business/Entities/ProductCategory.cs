﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BwalaBack.Business.Entities
{
    public class ProductCategory
    {
        public int ID { get; set; }
        public int? SuperID { get; set; }
        public string Name { get; set; }
    }
}
