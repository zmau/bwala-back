﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BwalaBack.Business.Entities
{
    public class BwalaContext : DbContext
    {
        public BwalaContext(DbContextOptions<BwalaContext> options)
                   : base(options)
        {
        }

        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
