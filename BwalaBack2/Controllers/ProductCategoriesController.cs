﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BwalaBack.Business.Entities;

namespace BwalaBack.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class ProductCategoriesController : Controller
    {
        [HttpGet]
        //[Authorize]
        public IEnumerable<ProductCategory> Get()
        {
            ProductCategory food = new ProductCategory
            {
                ID = 1,
                SuperID = null,
                Name = "food"
            };
            ProductCategory drinks = new ProductCategory
            {
                ID = 2,
                SuperID = null,
                Name = "drinks"
            };
            List<ProductCategory> list = new List<ProductCategory>();
            list.Add(food);
            list.Add(drinks);
            return list.ToArray();
        }
    }
}